import React, { useEffect, useState } from "react";
import { Button, Table } from "reactstrap";
import './TableListUkm.css';
import axios from "axios";
import { useNavigate } from "react-router";
import Coba from "../Coba";
import { Link } from "react-router-dom";

const TableListUkm = () => {

    const token = localStorage.getItem("token");
    const [daftarUsaha, setDaftarUsaha] = useState([]);
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        setisLoading(true);
        getUsaha();
    }, []);

    const getUsaha = () => {
        axios
            .get('http://api.kolektif-umkm.turbin.id/api/usaha',
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then((response) => {
                console.log(response.data);
                setDaftarUsaha(response.data);
                setisLoading(false);
            })
            .catch((err) => {
                // jika gagal
                console.log(err);
                setisError(true);
                setisLoading(false);

            });
    };

    const deleteUsaha = (id) => {
        console.log("user dengan id =" + id)
        axios
            .delete(`http://api.kolektif-umkm.turbin.id/api/usaha/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then((response) => {
                console.log(response.data);
            });


    }

    const getUsahaById = (id) => {
        console.log("edit user dengan id =" + id)
        axios
            .get(`http://api.kolektif-umkm.turbin.id/api/usaha-by-id/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then((response) => {
                console.log(response.data);

                // navigate('/edit-usaha')
            });
    }


    if (isLoading) return <p>Loading ...</p>;
    else if (daftarUsaha && !isError)
        return (
            <Table hover bordered
                className="tabel-ukm">
                <thead>
                    <tr>
                        <th>No </th>
                        <th>Nama Pemilik Usaha</th>
                        <th>Nama Usaha</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarUsaha.map((usaha, index) => {
                        return (
                            <tr key={usaha.id}>
                                <td>{index + 1}</td>
                                <td>{usaha.user.nama}</td>
                                <td>{usaha.jenis_usaha}</td>
                                <td>
                                    {/* <Button className="update" color="warning" outline onClick={() => updateUsaha(usaha.id)}>Edit</Button> */}
                                    <Link to={`/usaha/edit/${usaha.id}`}><Button className="update" color="warning" outline>Edit</Button></Link>
                                    <Button className="delete" color="danger" outline onClick={() => deleteUsaha(usaha.id)}>Delete</Button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        )
    else {
        return <p>Terjadi Kesalahan</p>;
    }
}
export default TableListUkm;