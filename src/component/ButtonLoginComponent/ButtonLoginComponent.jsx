import React, { useState } from 'react';
import { Button } from 'reactstrap';
import './ButtonLoginComponent.css';

const ButtonLoginComponent = () => {
    const [no_hp,setNo_hp] = useState('');
    const [password,Password] = useState('');

const submitLogin = () => {
    const data = {
        no_hp : no_hp,
        password : password
    }
    console.log(data)
}

return (
    <div className="button-login">
        <Button color="dark" onClick={submitLogin}>Login</Button>
    </div>
);
}

export default ButtonLoginComponent;