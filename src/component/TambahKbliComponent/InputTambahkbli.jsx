import React from 'react';
import { Input, Button } from 'reactstrap';
import './TambahKbliComponent.css'

const InputTambahKbli = (props) => {
    return (
        <div className="tambah-input">
            <Input
                className="tambah--input"
                type="text"
                placeholder="Kode KBLI">
            </Input>
            <Input
                className="tambah--input"
                type="text"
                placeholder="Nama Barang / Jasa"
                aria-label="" >
            </Input>
            <Button color="dark">Simpan Role</Button>
        </div>
    );
}
export default InputTambahKbli;

