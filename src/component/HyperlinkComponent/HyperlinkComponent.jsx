import React, { } from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import PasswordBaru from '../../container/PasswordBaru/PasswordBaru';
import './HyperlinkComponent.css';

const Hyperlink = (props) => {
    return (
        <div>
            <BrowserRouter>
                <ul>
                    <li><Link to={"/login"} className="menu" onmouseover="openCity(event, 'login')"></Link></li>
                    <li><Link to={"/password-baru"}>Lupa Password?</Link></li>
                </ul>
            </BrowserRouter>
        </div>
    );
}
export default Hyperlink;

