import React, { } from "react";
import { Table } from "reactstrap";
import ButtonAction from "../ButtonAction/ButtonAction";
import './MasterDataRoleComponent.css'

const TabelRole = () => (
    <Table hover bordered
        className="tabel-role"
    >
        <thead>
            <tr>
                <th>No </th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>User</td>
                <td>
                    <ButtonAction />
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Pendata</td>
                <td>
                    <ButtonAction />
                </td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Administrator</td>
                <td>
                    <ButtonAction />
                </td>
            </tr>
        </tbody>
    </Table>
)
export default TabelRole;