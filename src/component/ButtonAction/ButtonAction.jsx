import React, { } from "react";
import { Button } from "reactstrap";
import './ButtonAction.css';

const ButtonAction = (props) => {
    return (
        <div>
            <Button className="update" color="warning" outline onClick={() => props.update(props.usaha)}>Edit</Button>
            <Button className="delete" color="danger" outline onClick={() => props.delete(props.usaha)}>Delete</Button>
        </div>
    )
}
export default ButtonAction;