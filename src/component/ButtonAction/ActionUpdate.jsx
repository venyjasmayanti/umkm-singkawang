import React, { useEffect, useState } from 'react';
import { FormGroup, Form, Col, Input, Label, Button, Row } from 'reactstrap';
import axios from 'axios';
import { MultiUploader } from '../../container/Uploaders/Uploaders';
import '../../container/Pendata/Pendaftaran.css';


function ActionUpdate() {

    const [nik, setNik] = useState('');
    const [nama, setNama] = useState('');
    const [jenis_kelamin, setJenis_kelamin] = useState('');
    const [no_hp, setNo_hp] = useState('');
    const [alamat, setAlamat] = useState('');
    const [nama_usaha, setNama_usaha] = useState('');
    const [nib, setNib] = useState('');
    const [jenis_usaha, setJenis_usaha] = useState('');
    const [alamat_usaha, setAlamat_usaha] = useState('');
    const [aset, setAset] = useState('');
    const [rata_omset_perbulan, setRata_omset_perbulan] = useState('');
    const [karyawan_lk, setKaryawan_lk] = useState('');
    const [karyawan_pr, setKaryawan_pr] = useState('');
    const [tanggal, setTanggal] = useState('');
    const [iumk_nomor, setIumk_nomor] = useState('');
    const [kbli_id, setKbli_id] = useState('');
    const [jenis_badan_usaha, setJenis_badan_usaha] = useState('');
    const [logo, setLogo] = useState('');
    const [produk, setProduk] = useState('');
    const [lokasi, setLokasi] = useState('');


    const token = localStorage.getItem("token");

    const [bookRoomData, setBookRoomData] = useState([
        { roomIumk: '', roomDate: 0 }
    ]
    );

    const handleAddFields = () => {
        const values = [...bookRoomData];
        values.push({ roomIumk: '', roomDate: 0 })
        setBookRoomData(values);
    };

    useEffect(() => {

        getUsahaById();

    }, []);


    const getUsahaById = (id) => {
        console.log("edit user dengan id =" + id)
        axios
            .get(`http://api.kolektif-umkm.turbin.id/api/usaha-by-id/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then((response) => {
                console.log(response.data);
            });
    }


    const updateUsaha = (id) => {
        console.log("edit user dengan id =" + id)
        axios
            .post(`http://api.kolektif-umkm.turbin.id/api/usaha/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then((response) => {
                console.log(response.data);
                console.log("edit user dengan id =" + id)
            });


    }

    return (
        <div className="pendaftaran">
            <Form>

                <h1>Pendaftaran UKM Kota Singkawang</h1>
                <h2>A. Data Pemilik Usaha</h2>
                <FormGroup row>
                    <Label className="font-label"
                        sm={4}>
                        Nomor Induk Kependudukan (NIK)
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={nik}
                            onChange={(e) => setNik(e.target.value)}
                            id=""
                            name="nik"
                            placeholder="3301155706860002"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label className="font-label" sm={4} > Nama Pemilik (Usaha Sesuai KTP)</Label>
                    <Col sm={8}>
                        <Input
                            value={nama}
                            onChange={(e) => setNama(e.target.value)}
                            id=""
                            name="name"
                            placeholder="Slamet Kurniawan"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row tag="fieldset" className="font-label">
                    <legend className="col-form-label col-sm-4">
                        Jenis Kelamin
                    </legend>
                    <Col sm={8}>
                        <FormGroup check inline>
                            <Input
                                value={jenis_kelamin}
                                onChange={(e) => setJenis_kelamin(e.target.value)}
                                name="radio2"
                                type="radio"
                            />
                            {' '}
                            <Label check>
                                Laki-laki
                            </Label>
                        </FormGroup>
                        <FormGroup check inline>
                            <Input
                                value={jenis_kelamin}
                                onChange={(e) => setJenis_kelamin(e.target.value)}
                                name="radio2"
                                type="radio"
                            />
                            {' '}
                            <Label check>
                                Perempuan

                            </Label>
                        </FormGroup>

                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Alamat Tempat Tinggal
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={alamat}
                            onChange={(e) => setAlamat(e.target.value)}
                            id=""
                            name="alamat"
                            placeholder="Kalimantan, Kalimantan Barat, Kota Pontianak, Pontianak Selatan, Kota Baru, Jalan Prof.M.Yamin No.6, 78113 "
                            type="textarea"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        No. Handphone
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={no_hp}
                            onChange={(e) => setNo_hp(e.target.value)}
                            id=""
                            name="nohp"
                            placeholder="085346554998"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>

                <h2>B. Data Usaha</h2>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Nama Usaha :
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={nama_usaha}
                            onChange={(e) => setNama_usaha(e.target.value)}
                            id=""
                            name="name"
                            placeholder="Acan Petshop"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        NIB :
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={nib}
                            onChange={(e) => setNib(e.target.value)}
                            id=""
                            name="nib"
                            placeholder="032842739842"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Produk/Jenis Usaha
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={jenis_usaha}
                            onChange={(e) => setJenis_usaha(e.target.value)}
                            id=""
                            name="produk"
                            placeholder="Perindustrian, Jasa, Perdagangan"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>

                <Form>

                    {
                        bookRoomData.map((data, i) => {
                            return (

                                <><Row key={i}>
                                    <FormGroup row>
                                        <Label
                                            className="font-label"
                                            sm={4}
                                        >
                                            Perizinan yang dimiliki & no/tgl
                                        </Label>
                                        <Col className="colom-kbli" sm={8}>
                                            <Input
                                                value={kbli_id}
                                                onChange={(e) => setKbli_id(e.target.value)}
                                                name="perizinan"
                                                placeholder="KBLI"
                                                type="text"
                                                className="input-pendata" />

                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <legend className="col-form-label col-sm-3">

                                        </legend>
                                        <Col className="colom-date-iumk">
                                            <FormGroup check inline>
                                                <Input
                                                    value={tanggal}
                                                    onChange={(e) => setTanggal(e.target.value)}
                                                    id=""
                                                    name="date"
                                                    type="date"
                                                    placeholder="Tanggal"
                                                    className="input-pendata" />
                                            </FormGroup>


                                            <FormGroup check inline>
                                                <Input
                                                    value={iumk_nomor}
                                                    onChange={(e) => setIumk_nomor(e.target.value)}
                                                    id=""
                                                    name="iumk"
                                                    type="text"
                                                    placeholder="IUMK No."
                                                    className="input-pendata" />
                                            </FormGroup>
                                        </Col>
                                    </FormGroup>
                                </Row></>
                            )
                        })
                    }
                    <Row>
                        <Col className="tambah-perizinan">
                            <Button onClick={handleAddFields}>Tambah Perizinan</Button>

                        </Col>
                    </Row> <br />
                </Form>



                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Jenis Badan Usaha
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={jenis_badan_usaha}
                            onChange={(e) => setJenis_badan_usaha(e.target.value)}
                            id=""
                            name="select"
                            type="select"
                            className="input-pendata"
                        >
                            <option>
                                Individu
                            </option>
                            <option>
                                Kelompok
                            </option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Alamat Tempat Usaha
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={alamat_usaha}
                            onChange={(e) => setAlamat_usaha(e.target.value)}
                            id=""
                            name="alamat"
                            placeholder="Jalan Sultan Abdurrahman No.72, Akcaya, Pontianak Selatan, Sungai Bangong, Kec. Pontianak Kota, Kota Pontianak, Kalimantan Barat 78116"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Aset :
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={aset}
                            onChange={(e) => setAset(e.target.value)}
                            id=""
                            name="aset"
                            placeholder="bangunan, tanah"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Omset Rata-rata per Bulan
                    </Label>
                    <Col sm={8}>
                        <Input
                            value={rata_omset_perbulan}
                            onChange={(e) => setRata_omset_perbulan(e.target.value)}
                            id=""
                            name="omset"
                            placeholder="100.000.000 - 200.000.000"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row className="font-label">
                    <legend className="col-form-label col-sm-3">
                        Jumlah karyawan
                    </legend>

                    <Col className="colom-karyawan" sm={9}>
                        <FormGroup check inline>
                            <Input
                                value={karyawan_lk}
                                onChange={(e) => setKaryawan_lk(e.target.value)}
                                id=""
                                name="karyawan"
                                placeholder="Laki-laki"
                                type="text"
                                className="input-pendata"

                            />

                        </FormGroup>

                        <FormGroup check inline>

                            <Input
                                value={karyawan_pr}
                                onChange={(e) => setKaryawan_pr(e.target.value)}
                                id=""
                                name="karyawan"
                                placeholder="Perempuan"
                                type="text"
                                className="input-pendata"
                            />
                        </FormGroup>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <MultiUploader
                        value={logo}
                        onChange={(e) => setLogo(e.target.value)}
                        className="label-foto"
                        uploadUrl="images/multi-upload"
                        id="multi-uploader"
                        label="Foto Logo Usaha"
                    />
                </FormGroup>
                <FormGroup>
                    <MultiUploader
                        value={produk}
                        onChange={(e) => setProduk(e.target.value)}
                        className="label-foto"
                        uploadUrl="images/multi-upload"
                        id="multi-uploader"
                        label="Foto Produk (bisa lebih dari 1)"
                    />
                </FormGroup>
                <FormGroup>
                    <MultiUploader
                        value={lokasi}
                        onChange={(e) => setLokasi(e.target.value)}
                        className="label-foto"
                        uploadUrl="images/multi-upload"
                        id="multi-uploader"
                        label="Foto Lokasi"
                    />
                </FormGroup>
                <br />
                <FormGroup check row>
                    <Col className="button-simpan"
                    >

                        <Button onClick={updateUsaha}>Simpan UKM Baru</Button>


                    </Col>
                </FormGroup>


            </Form ></div >
    );
}



export default ActionUpdate;