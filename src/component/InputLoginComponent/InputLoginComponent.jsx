import React, { useEffect, useState } from 'react';
import { Input, Button } from 'reactstrap';
import './InputLoginComponent.css';
import axios from 'axios';
import { useNavigate } from 'react-router';
import { Link } from 'react-router-dom';

function InputLoginComponent() {
    const [no_hp, setNo_hp] = useState('');
    const [password, setPassword] = useState('');

    const [validation, setValidation] = useState([]);

    //define history
    const navigate = useNavigate()

    //hook useEffect
    useEffect(() => {

        //check token
        if (localStorage.getItem('token')) {

            //redirect page dashboard
            navigate('/pendaftaran');
        }
    }, []);

    const loginHandler = async (e) => {
        e.preventDefault();

        //initialize formData
        const formData = new FormData();

        //append data to formData
        formData.append('no_hp', no_hp);
        formData.append('password', password);

        //send data to server
        await axios.post('http://api.kolektif-umkm.turbin.id/api/login', formData)
            .then((response) => {

                console.log(response);

                //set token on localStorage
                localStorage.setItem('token', response.data.token);

                //redirect to dashboard
                navigate('/pendaftaran')

            })
            .catch((error) => {

                //assign error to state "validation"
                setValidation(error.response.data);
            })
    };

    return (
        <div className="login-input">
            {
                validation.message && (
                    <div className="alert alert-danger">
                        {validation.message}
                    </div>
                )
            }
            <form onSubmit={loginHandler}>
                <Input
                    className="form--input"
                    type="text"
                    value={no_hp}
                    placeholder="Masukkan no hp anda"
                    aria-label=""
                    onChange={(e) => setNo_hp(e.target.value)}>
                </Input>
                <Input
                    className="form--input"
                    type="password"
                    value={password}
                    name="password"
                    placeholder="Password"
                    onChange={(e) => setPassword(e.target.value)}>
                </Input>
                <Link 
                className='link' to={"/verifikasi-kode"}>Lupa Password?</Link><br />
                <Button type="submit" color="dark" className='btn'>Login</Button>
            </form>
        </div>
    );
}
export default InputLoginComponent;