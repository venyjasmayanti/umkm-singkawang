import React from 'react';
import { } from 'reactstrap';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Pendaftaran from '../../container/Pendata/Pendaftaran';
import PerbaruData from '../../container/Pelaku/PerbaruData';
import ListUkm from '../../container/ListUkm/ListUkm';
import PesanMasal from '../../container/PesanMasal/PesanMasal';
import DashBoard from '../../container/DashBoard/DashBoard';
import PasswordBaru from '../../container/PasswordBaru/PasswordBaru';
import NavBarPendata from '../../container/SideBarPendata/NavBarPendata';
import Login from '../../container/Login/Login';
import VerifikasiKode from '../../container/VerifikasiKode/VerifikasiKode';

import './Home.css';
import ActionUpdate from '../ButtonAction/ActionUpdate';
import Coba from '../Coba';

function Home() {
    const token = localStorage.getItem("token");
    console.log(token);


    return (

        <Router>
             {token ? <NavBarPendata/>  : ''}
            <Routes>
                <Route path="/pendaftaran" element={<Pendaftaran />} />
                <Route path="/pelaku" element={<PerbaruData />} />
                <Route path="/list-ukm" element={<ListUkm />} />
                <Route path="/pesan-masal" element={<PesanMasal />} />
                <Route path="/dashboard" element={<DashBoard />} />
                <Route path="/" element={<Login />} />
                <Route path="/password-baru" element={<PasswordBaru />} />
                <Route path="/verifikasi-kode" element={<VerifikasiKode />} />
                <Route path="/edit-usaha" element={<ActionUpdate />} />
                <Route path="/usaha/edit/:id" element={<Coba />} />
            </Routes >
        </Router>
    )
}
export default Home;