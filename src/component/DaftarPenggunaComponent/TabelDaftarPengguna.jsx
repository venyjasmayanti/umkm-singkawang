import React, { } from "react";
import { Table } from "reactstrap";
import ButtonAction from "../ButtonAction/ButtonAction";
import './DaftarPenggunaComponent.css'

const TabelDaftarPengguna = () => (
    <Table bordered
        className="daftar-pengguna"
    >
        <thead>
            <tr>
                <th>No </th>
                <th>Nama</th>
                <th>Role</th>
                <th>No. HP</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Eleanor Pena</td>
                <td>User</td>
                <td>+62-838-5552-72</td>
                <td>
                    <ButtonAction />
                </td>
            </tr>
        </tbody>
    </Table>
)
export default TabelDaftarPengguna;