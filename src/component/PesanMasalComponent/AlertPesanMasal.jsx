import React, { } from "react";
import { Alert, Input, Button } from "reactstrap";
import './PesanMasalComponent.css'

const AlertPesanMasal = () => (
    <div className="alert-pesan">
        <Alert>
            <h1>
                Pesan yang ingin dikirim
            </h1>
            <Input
                name="text"
                type="textarea"
                placeholder='"Diingatkan sekali lagi untuk Diupdate datanya"'
            />
            <hr />
            <Button color="dark">Submit Pesan</Button>
        </Alert>
    </div>
)
export default AlertPesanMasal;