import React, { useEffect, useState } from "react";
import { Input, Table } from "reactstrap";
import './PesanMasalComponent.css';
import axios from "axios";

const TabelPesanMasal = () => {

    const token = localStorage.getItem("token");
    const [daftarUsaha, setDaftarUsaha] = useState([]);
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);

    useEffect(() => {
        setisLoading(true);
        axios
            .get('http://api.kolektif-umkm.turbin.id/api/usaha',
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then((response) => {
                console.log(response.data);
                setDaftarUsaha(response.data);
                setisLoading(false);
            })
            .catch((err) => {
                // jika gagal
                console.log(err);
                setisError(true);
                setisLoading(false);

            });

    }, []);

    if (isLoading) return <p>Loading ...</p>;
    else if (daftarUsaha && !isError)
        return (
            <Table hover bordered
                className="pesan-masal"
            >
                <thead>
                    <tr>
                        <th>No </th>
                        <th>Nama Pemilik Usaha</th>
                        <th>Nama Usaha</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {daftarUsaha.map((usaha, index) => {
                        return (
                            <tr key={usaha.id}>
                                <td>{index + 1}</td>
                                <td>{usaha.user.nama}</td>
                                <td>{usaha.jenis_usaha}</td>
                                <td ><Input className="checkPesan" type="checkbox" /></td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        )
    else {
        return <p>Something Went Wrong</p>;
    }
}
export default TabelPesanMasal;