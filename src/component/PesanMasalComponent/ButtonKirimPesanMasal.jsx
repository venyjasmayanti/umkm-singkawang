import React, { } from "react";
import { Button } from "reactstrap";
import './PesanMasalComponent.css'


const ButtonKirimPesanMasal = () => (
    <div>
        <Button className="button-kirim" color="dark"
            onClick={() => {
                let pesan = prompt(
                    "Pesan yang ingin dikirim",
                );
                if (pesan) {
                    alert(pesan);
                } else {
                    console.log('Cancel');
                }
            }}>Kirim Pesan</Button>
    </div>
)

export default ButtonKirimPesanMasal;