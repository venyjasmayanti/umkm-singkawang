import React, { } from "react";
import { Table } from "reactstrap";
import ButtonAction from "../ButtonAction/ButtonAction";
import './MasterDataKbliComponent.css'

const TabelKbli = () => (
    <Table hover bordered
        className="tabel-Kbli"
    >
        <thead>
            <tr>
                <th>No </th>
                <th>Kode KBLI</th>
                <th>Nama Barang/Jasa Dagang</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>01111</td>
                <td>Pertanian Jagung</td>
                <td>
                    <ButtonAction />
                </td>
            </tr>
        </tbody>
    </Table>
)
export default TabelKbli;