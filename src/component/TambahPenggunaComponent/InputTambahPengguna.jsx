import React from 'react';
import { Input, Button } from 'reactstrap';
import './TambahPenggunaComponent.css'

const InputTambahPengguna = (props) => {
    return (
        <div className="tambah-input">
            <Input
                className="tambah--input"
                type="text"
                placeholder="Nama">
            </Input>
            <Input
                className="tambah--input"
                type="text"
                placeholder="NIK"
                aria-label="" >
            </Input>
            <Input
                className="tambah--input"
                type="email"
                name="email"
                placeholder="Email">
            </Input>
            <Input
                className="tambah--input"
                id="exampleSelectMulti"
                name="selectMulti"
                type="select"
                placeholder="Role"
            >
                <option>
                    User
                </option>
                <option>
                    Pendata
                </option>
                <option>
                    Admin
                </option>
            </Input>
            <Input
                className="tambah--input"
                type="text"
                name="text"
                placeholder="No Hp">
            </Input>
            <Input
                className="tambah--input"
                type="password"
                name="password"
                placeholder="Password">
            </Input>
            <Button color="dark">Simpan Pengguna</Button>
        </div>
    );
}
export default InputTambahPengguna;


