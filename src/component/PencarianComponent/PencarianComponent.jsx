import React, { } from "react";
import { Input } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import './PencarianComponent.css'

const PencarianComponent = () => (
    <div>
        <FontAwesomeIcon className="icon-search" icon={faSearch} />
        <Input
            className="cari-input"
            type="text"
            placeholder="Cari nama"
            aria-label="" >
        </Input>
    </div>
)

export default PencarianComponent;