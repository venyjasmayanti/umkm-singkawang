import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Input } from 'reactstrap';
import CountDownComponent from './CountDownComponent';
import './VerifikasiKodeComponent.css'

const VerifikasiKodeComponent = (props) => {
    return (
        <div className="kode-otp">
            <h1>Verifikasi Kode</h1>
            <p>Silahkan dicek SMS atau Email yang Kami Kirim</p>
            <Input
                name="otp1"
                type="text"
                autoComplete="off"
                className="otp-input"
            />
            <Input
                name="otp2"
                type="text"
                autoComplete="off"
                className="otp-input"
            />
            <Input
                name="otp3"
                type="text"
                autoComplete="off"
                className="otp-input"
            />
            <Input
                name="otp4"
                type="text"
                autoComplete="off"
                className="otp-input"
            /><br />
            <Link
                className='link-kirim-ulang' to={""}>Kirim Ulang</Link><br />
            <CountDownComponent /><br />
            <Button color="dark">Konfirmasi</Button>
        </div>
    );
}
export default VerifikasiKodeComponent;

