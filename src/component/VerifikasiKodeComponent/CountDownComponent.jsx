import React from 'react';
import Countdown from 'react-countdown';

const CountDownComponent = (props) => {
    return (
        <Countdown
            className="count-down"
            date={Date.now() + 30000}
        >
        </Countdown>
    );
}
export default CountDownComponent;