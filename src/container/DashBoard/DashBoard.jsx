import React, { } from 'react';
import { Bar, Doughnut, Line, Pie } from 'react-chartjs-2';
import Chart from 'chart.js/auto';
import './DashBoard.css';

function DashBoard() {
    return (
        <div className="dashboard">
            <h1>Dashboard UKM</h1>
            <div className='chart-1'>
                <div className='bar-chart'>
                    <Bar
                        data={{
                            labels: ['< 100jt', '100 - 200jt', '200 - 300jt', '300 - 500jt'],
                            datasets: [{
                                label: 'Omset Perbulan UKM',
                                data: [80, 160, 230, 500],
                                backgroundColor: [
                                    '#11a4d1',
                                ],
                                borderColor:['black'],
                                borderWidth:1
                            }],
                        }}
                        width={400}
                        height={400}
                        options={{ 
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                         }}
                    >
                    </Bar>
                </div>

                <div className='line-chart'>
                    <Line
                        data={{
                            labels: ['PT', 'CV', 'Koperasi', 'Perorangan'],
                            datasets: [{
                                label: ['Jenis - jenis Badan Usaha'],
                                data: ['200', '100', '160', '120', '300'],
                                backgroundColor: ['black'],
                                borderColor:['green'],
                                borderWidth:4,
                            }]
                        }}
                        width={400}
                        height={400}
                        options={{ 
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                         }}
                    >
                    </Line>
                </div>
            </div>

            <div className='chart-2'>
                <div className='pie-chart'>
                    <Pie
                        data={{
                            labels: ['UKM Pria', 'UKM Wanita'],
                            datasets: [{
                                label: 'Omset Perbulan UKM',
                                data: [400, 600],
                                backgroundColor: [
                                    '#4747d1', '#5cd6d6',
                                ],
                                borderWidth: 2,
                            }]
                        }}
                        width={400}
                        height={400}
                        options={{ 
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                         }}
                    >
                    </Pie>
                </div>

                <div className='doughnut-chart'>
                    <Doughnut
                        data={{
                            labels: ['Pekerja Pria', 'Pekerja Wanita'],
                            datasets: [{
                                label: 'Omset Perbulan UKM',
                                data: [400, 600],
                                backgroundColor: [
                                    '#80ff80', '#ff6666',
                                    'rgba(54, 162, 235, 0.2)',
                                ],
                                borderWidth: 2,
                            }]
                        }}
                          width={400}
                        height={400}
                        options={{ 
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                         }}
                    >
                    </Doughnut>
                </div>
            </div>
        </div>

    );
}


export default DashBoard;
