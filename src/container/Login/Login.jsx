import React, { Fragment } from 'react';
// import ButtonComponent from '../../component/ButtonLoginComponent/ButtonLoginComponent';
// import HyperlinkComponent from '../../component/HyperlinkComponent/HyperlinkComponent';
import InputComponent from '../../component/InputLoginComponent/InputLoginComponent';
import './Login.css';

function Login() {
    return (
        <Fragment>
            <div className="form-login">
                <h2>Pendaftaran UMKM Kota Singkawang</h2>
                <InputComponent />
                {/* <HyperlinkComponent />
                <ButtonComponent /> */}
            </div>
        </Fragment>
    )
}
export default Login;