import React, { } from "react";
import ButtonKirimKbli from "../../component/MasterDataKbliComponent/ButtonKirimKbli";
import TabelKbli from "../../component/MasterDataKbliComponent/TabelKbli";
import PaginationComponent from "../../component/PaginationComponent/PaginationComponent";
import PencarianComponent from "../../component/PencarianComponent/PencarianComponent";
import './MasterKbli.css';

function MasterDataKbli() {
    return (
        <div>
            <h1>Master Data Kbli</h1>
            <div class="kbli">
                <PencarianComponent/> 
                <ButtonKirimKbli/>
                <TabelKbli/>
                <PaginationComponent/>
            </div>
        </div>
    )

}
export default MasterDataKbli;