// import React, { Fragment } from "react";
// import { NavLink } from "react-router-dom";
// // import './SideBarPendata.css';
// import { FaBars } from 'react-icons/fa';
// import Pendaftaran from "../Pendata/Pendaftaran";


// function SideBarPendata() {

//     return (
//         <ul className="sidebar">
//             <FaBars className="icon"/>
//             <div className="container">
//                 <li><NavLink to={"/pendaftaran"} className="menu">Pendaftaran UKM</NavLink></li>
//                 <li><NavLink to={"/list-ukm"} className="menu">Data List UKM</NavLink></li>
//                 <li><NavLink to={"/pesan-masal"} className="menu">Kirim Pesan Masal</NavLink></li>
//                 <li><NavLink to={"/dashboard"} className="menu">Dashboard</NavLink></li>
//             </div>
//         </ul>
//     );
// }

// export default SideBarPendata;

export const SideBarPendata = [
    {
        title: 'Pendaftaran UKM',
        path:'/pendaftaran',
        cName:'nav-text'
    },
    {
        title: 'Data List UKM',
        path:'/list-ukm',
        cName:'nav-text'
    },
    {
        title: 'Kirim Pesan Masal',
        path:'/pesan-masal',
        cName:'nav-text'
    },
    {
        title: 'Dashboard',
        path:'/dashboard',
        cName:'nav-text'
    },
]

