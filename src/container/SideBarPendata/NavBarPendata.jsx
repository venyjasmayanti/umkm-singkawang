import React, { useState } from "react";
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { NavLink } from 'react-router-dom';
import { SideBarPendata } from "./SideBarPendata";
import './NavBarPendata.css';
import { IconContext } from 'react-icons'
import { Button } from "reactstrap";
import axios from "axios";
import { useNavigate } from 'react-router';

function NavBarPendata() {
    const [sidebar, setSidebar] = useState(false);

    const showSidebar = () => setSidebar(!sidebar);

    const navigate = useNavigate();
    const token = localStorage.getItem("token");

    //function logout
    const logoutHandler = async () => {
        axios.post('http://api.kolektif-umkm.turbin.id/api/logout',
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(() => {
                localStorage.removeItem("token");
                navigate('/');
            });
    };

    return (
        <>
            <IconContext.Provider value={{ color: '#343636' }}>
                <div className="navbar">
                    <NavLink to="#" className="menu-bars">
                        <FaIcons.FaBars onClick={showSidebar} />
                    </NavLink>

                    {/* Membuat button LogOut */}
                    <Button className="logout" onClick={logoutHandler} outline>Log out</Button>
                </div>


                <nav className={sidebar ? 'nav-menu active' : 'nav-menu'} >
                    <ul className="nav-menu-items" onClick={showSidebar}>
                        <li className="navbar-toggle">
                            <NavLink to="#" className="menu-bars">
                                <AiIcons.AiOutlineClose className="icon-close" />
                            </NavLink>
                        </li>
                        {SideBarPendata.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <NavLink to={item.path}>
                                        <span>{item.title}</span>
                                    </NavLink>
                                </li>
                            )
                        })}
                    </ul>

                </nav>
            </IconContext.Provider>
        </>
    )
}

export default NavBarPendata;