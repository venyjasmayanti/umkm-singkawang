import React from 'react';
import './SideBarManajemenPengguna.css';

const SideBarManajemenPengguna = () => {
    return (
        <div>
            <ul class="sidebar">
                <li className="top"><a href="/form">Daftar Pengguna</a></li>
                <li className='dropdown'>
                    <a href="#" className='dropbtn'>Manajemen Master Data</a></li>
            </ul>
        </div>
    );
}

export default SideBarManajemenPengguna;