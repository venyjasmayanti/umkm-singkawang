import React, { } from "react";
import ButtonKirimRole from "../../component/MasterDataRoleComponent/ButtonKirimRole";
import TabelRole from "../../component/MasterDataRoleComponent/TabelRole";
import PaginationComponent from "../../component/PaginationComponent/PaginationComponent";
import PencarianComponent from "../../component/PencarianComponent/PencarianComponent";
import './MasterRole.css';

function MasterDataRole() {
    return (
        <div>
            <h1>Master Data Role</h1>
            <div class="role">
                <PencarianComponent/> 
                <ButtonKirimRole/><br/>
                <TabelRole/>
                <PaginationComponent/>
            </div>
        </div>
    )

}
export default MasterDataRole;