import React, {Fragment} from 'react';
import VerifikasiKodeComponent from '../../component/VerifikasiKodeComponent/VerifikasiKodeComponent';

function VerifikasiKode() {
    return (
        <Fragment>
            <div className="form-verifikasi-kode">
                <VerifikasiKodeComponent/>
            </div>
        </Fragment>
    )
}
export default VerifikasiKode;
