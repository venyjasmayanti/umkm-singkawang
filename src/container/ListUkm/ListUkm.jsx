import React, { } from "react";
import TabelDataListukm from "../../component/ListUkmComponent/TableListUkm";
import PaginationComponent from "../../component/PaginationComponent/PaginationComponent";
import PencarianComponent from "../../component/PencarianComponent/PencarianComponent";
import './ListUkm.css';

function ListUkm() {
    return (
        <div className="ukm">
            <h1>Data List UKM</h1>
            <div class="list-ukm">
                <PencarianComponent/>
                <TabelDataListukm/>
                <PaginationComponent/>
            </div>
        </div>
    )

}
export default ListUkm;