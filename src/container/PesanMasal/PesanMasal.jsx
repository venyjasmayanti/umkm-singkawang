import React, { } from "react";
import PaginationComponent from "../../component/PaginationComponent/PaginationComponent";
import PencarianComponent from "../../component/PencarianComponent/PencarianComponent";
import ButtonKirimPesanMasal from "../../component/PesanMasalComponent/ButtonKirimPesanMasal";
import TabelPesanMasal from "../../component/PesanMasalComponent/TabelPesanMasal";
import './PesanMasal.css';

function PesanMasal() {
    return (
        <div className="pesan">
            <h1>Kirim Pesan Masal</h1>
            <PencarianComponent/>
            <div class="pesan-masal"> 
                <ButtonKirimPesanMasal/><br/>
                <TabelPesanMasal/>
                <PaginationComponent/>
            </div>
        </div>
    )

}
export default PesanMasal;