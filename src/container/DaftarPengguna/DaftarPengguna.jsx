import React, { } from "react";
import ButtonKirimDaftarPengguna from "../../component/DaftarPenggunaComponent/ButtonKirimDaftarPengguna";
import TabelDaftarPengguna from "../../component/DaftarPenggunaComponent/TabelDaftarPengguna";
import PaginationComponent from "../../component/PaginationComponent/PaginationComponent";
import PencarianComponent from "../../component/PencarianComponent/PencarianComponent";
import './DaftarPengguna.css';

function DaftarPengguna() {
    return (
        <div>
            <h1>Kirim Pesan Masal</h1>
            <div class="daftar-pengguna">
                <PencarianComponent/> 
                <ButtonKirimDaftarPengguna/><br/>
                <TabelDaftarPengguna/>
                <PaginationComponent/>
            </div>
        </div>
    )

}
export default DaftarPengguna;