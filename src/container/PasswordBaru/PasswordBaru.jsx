import React, {} from 'react';
import ButtonPasswordBaruComponent from '../../component/PasswordBaruComponent/ButtonPasswordBaruComponent';
import InputPasswordBaruComponent from '../../component/PasswordBaruComponent/InputPasswordBaruComponent';
import './PasswordBaru.css';

function PasswordBaru() {
    return (
        <div className="form-password-baru">
            <h1>Membuat Password baru</h1>
            <InputPasswordBaruComponent/>
            <ButtonPasswordBaruComponent/>
        </div>

    );
}
export default PasswordBaru;
