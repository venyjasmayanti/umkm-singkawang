import React, { } from "react";
import InputTambahPengguna from "../../component/TambahPenggunaComponent/InputTambahPengguna";

function TambahPengguna() {
    return (
        <div>
            <h1>Penambahan Pengguna</h1>
            <div class="tambah-pengguna">
                <InputTambahPengguna/>
            </div>
        </div>
    )

}
export default TambahPengguna;