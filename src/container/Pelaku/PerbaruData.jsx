import React, { useState } from 'react';
import { FormGroup, Form, Col, Input, Label, Button, Row } from 'reactstrap';
import './PerbaruData.css';
import { MultiUploader } from '../Uploaders/Uploaders';



const PerbaruData = () => {

    const [bookRoomData, setBookRoomData] = useState([
        { roomIumk: '', roomDate: 0 }
    ]);

    const [roomIumk, setRoomIumk] = useState('');
    const [roomDate, setRoomDate] = useState(1);

    const handleChange = (index, event) => {
        const values = [...bookRoomData];
        if (event.target.name === 'roomIumk') {
            values[index].roomIumk = event.target.value
        } else if (event.target.name === 'roomDate' && event.target.value > 0) {
            values[index].roomDate = event.target.value
        }

        setBookRoomData(values)
    }


    const handleAddFields = () => {
        const values = [...bookRoomData];
        values.push({ roomIumk: '', roomDate: 0 })
        setBookRoomData(values);
    };


    return (
        <div className="daftar">
            <Form>

                <h1>Memperbaru data UMKM</h1>
                <h2>A. Data Pemilik Usaha</h2>
                <FormGroup row>
                    <Label className="font-label"
                        sm={4}>
                        Nomor Induk Kependudukan (NIK)
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="nik"
                            placeholder="3301155706860002"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label className="font-label" sm={4} > Nama Pemilik (Usaha Sesuai KTP)</Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="name"
                            placeholder="Slamet Kurniawan"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row tag="fieldset" className="font-label">
                    <legend className="col-form-label col-sm-4">
                        Jenis Kelamin
                    </legend>
                    <Col sm={8}>
                        <FormGroup check inline>
                            <Input
                                name="radio2"
                                type="radio"
                            />
                            {' '}
                            <Label check>
                                Laki-laki
                            </Label>
                        </FormGroup>
                        <FormGroup check inline>
                            <Input
                                name="radio2"
                                type="radio"
                            />
                            {' '}
                            <Label check>
                                Perempuan

                            </Label>
                        </FormGroup>

                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Alamat Tempat Tinggal
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="alamat"
                            placeholder="Kalimantan, Kalimantan Barat, Kota Pontianak, Pontianak Selatan, Kota Baru, Jalan Prof.M.Yamin No.6, 78113 "
                            type="textarea"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        No Handphone
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="nohp"
                            placeholder="085346554998"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>

                <h2>B. Data Usaha</h2>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Nama Usaha :
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="name"
                            placeholder="Acan Petshop"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        NIB :
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="nib"
                            placeholder="032842739842"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Produk/Jenis Usaha
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="produk"
                            placeholder="Perindustrian, Jasa, Perdagangan"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>

                <Form>

                    {
                        bookRoomData.map((data, i) => {
                            return (

                                <><Row key={i}>
                                    <FormGroup row>
                                        <Label
                                            className="font-label"
                                            sm={4}
                                        >
                                            Perizinan yang dimiliki & no/tgl
                                        </Label>
                                        <Col className="colom-kbli" sm={8}>
                                            <Input
                                                name="perizinan"
                                                placeholder="KBLI"
                                                type="text"
                                                className="input-pendata" />

                                        </Col>
                                    </FormGroup>
                                    <FormGroup row >
                                        
                                        <Row className="colom-input" >
                                            <Col className="colom-date" xs={8}>
                                                <FormGroup controlId="formRoomDate">
                                                    <Input
                                                        id=""
                                                        name="date"
                                                        type="date"
                                                        placeholder="Tanggal"
                                                        className="input-pendata" />
                                                </FormGroup>
                                            </Col>

                                            <Col className="colom-iumk" xs={4}>
                                                <FormGroup controlId="formRoomIumk">
                                                    <Input
                                                        id=""
                                                        name="iumk"
                                                        type="text"
                                                        placeholder="IUMK No."
                                                        className="input-pendata" />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </FormGroup>
                                </Row></>
                            )
                        })
                    }
                    <Row>
                        <Col className="tambah-perizinan">
                            <Button onClick={handleAddFields}>Tambah Perizinan</Button>

                        </Col>
                    </Row> <br />



                </Form>



                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Jenis Badan Usaha
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="select"
                            type="select"
                            className="input-pendata"
                        >
                            <option>
                                Individu
                            </option>
                            <option>
                                Kelompok
                            </option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Alamat Tempat Usaha
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="alamat"
                            placeholder="Jalan Sultan Abdurrahman No.72, Akcaya, Pontianak Selatan, Sungai Bangong, Kec. Pontianak Kota, Kota Pontianak, Kalimantan Barat 78116"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Aset :
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="aset"
                            placeholder="bangunan, tanah"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        className="font-label"
                        sm={4}
                    >
                        Omset Rata-rata per Bulan
                    </Label>
                    <Col sm={8}>
                        <Input
                            id=""
                            name="omset"
                            placeholder="100.000.000 - 200.000.000"
                            type="text"
                            className="input-pendata"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row className="font-label">
                    <legend className="col-form-label col-sm-3">
                        Jumlah karyawan
                    </legend>

                    <Col className="colom-karyawan" sm={9}>
                        <FormGroup check inline>
                            <Input
                                id=""
                                name="karyawan"
                                placeholder="Laki-laki"
                                type="text"
                                className="input-pendata"

                            />

                        </FormGroup>

                        <FormGroup check inline>

                            <Input
                                id=""
                                name="karyawan"
                                placeholder="Perempuan"
                                type="text"
                                className="input-pendata"
                            />
                        </FormGroup>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <MultiUploader
                        className="label-foto"
                        uploadUrl="images/multi-upload"
                        id="multi-uploader"
                        label="Foto Logo Usaha"
                    />
                </FormGroup>
                <FormGroup>
                    <MultiUploader
                        className="label-foto"
                        uploadUrl="images/multi-upload"
                        id="multi-uploader"
                        label="Foto Produk (bisa lebih dari 1)"
                    />
                </FormGroup>
                <FormGroup>
                    <MultiUploader
                        className="label-foto"
                        uploadUrl="images/multi-upload"
                        id="multi-uploader"
                        label="Foto Lokasi"
                    />
                </FormGroup>
                <br />
                <FormGroup check row>
                    <Col className="button-update"
                    >
                        <Button onClick={() => {
                            alert("Data berhasil diupdate")
                        }}>
                            Perbaharui Data UKM
                        </Button>



                    </Col>
                </FormGroup>


            </Form></div>
    );
}



export default PerbaruData;